import styled from '@emotion/styled'

const SongRowContainer = styled.div`
  position: relative;
  overflow: hidden;
`

export {
  SongRowContainer
}
